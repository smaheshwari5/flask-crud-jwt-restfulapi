import resource
from flask import Flask
from flask_restful import Resource, Api, reqparse, abort , fields, marshal_with
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///sqlite.db'
db = SQLAlchemy(app)


class ToDoModel(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    task = db.Column(db.String(200))
    summary = db.Column(db.String(500))

# db.create_all()


    
# todos = {
#     1: {"task": "write hello word program", "summary": "write the code using python !!" },
#     2: {"task": "Task 2", "summary": "writting task 2."},
#     3: {"task": "Task 3", "summary": "This is task 3."},

# }

task_post_args = reqparse.RequestParser()
task_post_args.add_argument("task", type=str, help="Task is required", required= True)
task_post_args.add_argument("summary", type=str, help="Summary is required", required= True)


task_put_args= reqparse.RequestParser()
task_put_args.add_argument("task", type=str)
task_put_args.add_argument("summary", type=str)


resource_fields= {
    'id': fields.Integer,
    'task': fields.String,
    'summary': fields.String,
}


# task_patch_args = reqparse.RequestParser()
# task_patch_args.add_argument("task", type=str, help="Task is required", required= True)
# task_patch_args.add_argument("summary", type=str, help="Summary is required", required= True)


class ToDoList(Resource):
    def get(self):
        tasks = ToDoModel.query.all()
        todos= {}
        for task in tasks:
            todos[task.id] = {"task": task.task, "summary": task.summary}
        return todos

        # def get(self):
    #     return todos

    


     
class ToDo(Resource):
    marshal_with(resource_fields)
    def get(self, todo_id):
        task = ToDoModel.query.filter_by(id=todo_id).first()
        if not task:
            abort(404, message="could not find task with that id ")
        return task

        # return todos[todo_id]

    

    marshal_with(resource_fields)
    def post(self, todo_id):
        args = task_post_args.parse_args()
        task = ToDoModel.query.filter_by(id=todo_id).first()
        if task:
            abort(409, message="task is taken -----")
        todo = ToDoModel(id = todo_id, task=args["task"], summary=args["summary"])
        db.session.add(todo)
        db.session.commit()
        return todo, 201


    # def post(self, todo_id):
        # args = task_post_args.parse_args()
        # if todo_id in todos:
        #     abort(409,"task id is already taken")
        # todos[todo_id] = {"task": args["task"], "summary": args["summary"]}
        # return todos[todo_id]



    marshal_with(resource_fields)
    def put(self, todo_id):
        args= task_put_args.parse_args()
        task = ToDoModel.query.filter_by(id=todo_id).first()
        if not task:
            abort(404, message="task doesn't exist, cannot update")
        if args["task"]:
            task.task = args["task"]
        if args["summary"]:
            task.summary = args["summary"]
        db.session.commit()
        return task

        # args = task_put_args.parse_args()
        # if todo_id not in todos:
        #     abort(404, message="Task doesn't exist, cannot update.")
        # if args['task']:
        #     todos[todo_id]["task"]= args['task']
        # if args['summary']:
        #     todos[todo_id]["summary"] = args['summary']
        # return todos[todo_id]

        


    # marshal_with(resource_fields)
    # def patch(self, todo_id):
    #     args = task_patch_args.parse_args()
    #     if todo_id not in todos:
    #         abort(404, message="Task doesn't exist, cannot update.")
    #     todos[todo_id] = {"task": args["task"], "summary": args["summary"]}   
    #     return todos[todo_id]


    def delete(self, todo_id):
        task = ToDoModel.query.filter_by(id=todo_id).first()
        db.session.delete(task)
        return "ToDo Deleted ", 204


        # del todos[todo_id]
        # return todos
        


api.add_resource(ToDo, '/todos/<int:todo_id>')
api.add_resource(ToDoList, '/todos')

# class Helloworld(Resource):
#     def get(self):
#         return {'data': 'Hello world'}


# class Helloname(Resource):
#     def get(self, name):
#         return {'data': 'HEllo, {}' .format(name)}


# api.add_resource(Helloworld, "/helloworld")
# api.add_resource(Helloname, "/helloworld/<string:name>")




if __name__== '__main__':
    app.run(debug=True)
