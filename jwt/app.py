from flask import Flask, request, jsonify, make_response, render_template, session
import jwt
# from email.message import _PayloadType
from datetime import datetime, timedelta
from functools import wraps

app = Flask(__name__)
app.config["SECRET_KEY"]= 'secretkey'


def token_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):

        token = request.args.get('token')
        if not token:
            return jsonify({'Alert':'Today is missing !!!' })
        
        try:
            payload = jwt.decode(token, app.config['SECRET_KEY'])
            # return jwt.decode(token, app.config['SECRET_KEY'])
        except:
            return jsonify({'Alert!!': 'Invalid Token !!'})


    return decorated


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')

    else:
        return 'Logged in currently !!'
    


@app.route('/public')
def public():
    return 'For Public '


@app.route('/auth')
@token_required
def auth():
    return 'JWT is verified '



@app.route('/login', method=['POST'])
def login():
    if request.form['username']and request.form['password']=='password':
        session['logged_in']= True
        token = jwt.encode({
            'user': request.form['username'],
            'expiration': str(datetime.utcnow() + timedelta(seconds=120))

        },
            app.config['SECRET_KEY'])
        return jsonify({'token': token.decode('utf-8')})

    else:
        return make_response('Unable to verify', 403, {'WWW-Authenticate': 'Basic realm:"Authntication Failed !! '})


if __name__=="__main__":
    app.run(debug=True)